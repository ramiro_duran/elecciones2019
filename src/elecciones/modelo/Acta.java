/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package elecciones.modelo;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author RDuran
 */
@Entity
@Table(name = "acta")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Acta.findAll", query = "SELECT a FROM Acta a"),
    @NamedQuery(name = "Acta.findById", query = "SELECT a FROM Acta a WHERE a.id = :id"),
    @NamedQuery(name = "Acta.findByNromesa", query = "SELECT a FROM Acta a WHERE a.nromesa = :nromesa"),
    @NamedQuery(name = "Acta.findByInscritos", query = "SELECT a FROM Acta a WHERE a.inscritos = :inscritos"),
    @NamedQuery(name = "Acta.findByCc", query = "SELECT a FROM Acta a WHERE a.cc = :cc"),
    @NamedQuery(name = "Acta.findByFpv", query = "SELECT a FROM Acta a WHERE a.fpv = :fpv"),
    @NamedQuery(name = "Acta.findByMts", query = "SELECT a FROM Acta a WHERE a.mts = :mts"),
    @NamedQuery(name = "Acta.findByUcs", query = "SELECT a FROM Acta a WHERE a.ucs = :ucs"),
    @NamedQuery(name = "Acta.findByMas", query = "SELECT a FROM Acta a WHERE a.mas = :mas"),
    @NamedQuery(name = "Acta.findByF21", query = "SELECT a FROM Acta a WHERE a.f21 = :f21"),
    @NamedQuery(name = "Acta.findByPdc", query = "SELECT a FROM Acta a WHERE a.pdc = :pdc"),
    @NamedQuery(name = "Acta.findByMnr", query = "SELECT a FROM Acta a WHERE a.mnr = :mnr"),
    @NamedQuery(name = "Acta.findByPan", query = "SELECT a FROM Acta a WHERE a.pan = :pan"),
    @NamedQuery(name = "Acta.findByValidos", query = "SELECT a FROM Acta a WHERE a.validos = :validos"),
    @NamedQuery(name = "Acta.findByBlancos", query = "SELECT a FROM Acta a WHERE a.blancos = :blancos"),
    @NamedQuery(name = "Acta.findByNulos", query = "SELECT a FROM Acta a WHERE a.nulos = :nulos"),
    @NamedQuery(name = "Acta.findByEstado", query = "SELECT a FROM Acta a WHERE a.estado = :estado"),
    @NamedQuery(name = "Acta.findByPais", query = "SELECT a FROM Acta a WHERE a.pais = :pais"),
    @NamedQuery(name = "Acta.findByDepartamento", query = "SELECT a FROM Acta a WHERE a.departamento = :departamento"),
    @NamedQuery(name = "Acta.findByProvincia", query = "SELECT a FROM Acta a WHERE a.provincia = :provincia"),
    @NamedQuery(name = "Acta.findByMunicipio", query = "SELECT a FROM Acta a WHERE a.municipio = :municipio"),
    @NamedQuery(name = "Acta.findByLocalidad", query = "SELECT a FROM Acta a WHERE a.localidad = :localidad")
    })
public class Acta implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)        
    private Integer id;
    @Column(name = "nromesa")
    private Double nromesa;
    @Column(name = "inscritos")
    private Double inscritos;
    @Column(name = "cc")
    private Double cc;
    @Column(name = "fpv")
    private Double fpv;
    @Column(name = "mts")
    private Double mts;
    @Column(name = "ucs")
    private Double ucs;
    @Column(name = "mas")
    private Double mas;
    @Column(name = "f21")
    private Double f21;
    @Column(name = "pdc")
    private Double pdc;
    @Column(name = "mnr")
    private Double mnr;
    @Column(name = "pan")
    private Double pan;
    @Column(name = "validos")
    private Double validos;
    @Column(name = "blancos")
    private Double blancos;
    @Column(name = "nulos")
    private Double nulos;
    @Column(name = "estado")
    private String estado;
    @Column(name = "pais")
    private String pais;
    @Column(name = "departamento")
    private String departamento;
    @Column(name = "provincia")
    private String provincia;
    @Column(name = "mncpo")
    private String mncpo;
    @Column(name = "localidad")
    private String localidad;
    @Column(name = "rcnto")
    private String rcnto;
    
    @JoinColumn(name = "id_recinto", referencedColumnName = "id")
    @ManyToOne
    private Recinto recinto;
    
    @JoinColumn(name = "id_municipio", referencedColumnName = "id")
    @ManyToOne
    private Municipio municipio;
    
    public Acta() {
    }

    public Acta(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Double getNromesa() {
        return nromesa;
    }

    public void setNromesa(Double nromesa) {
        this.nromesa = nromesa;
    }

    public Double getInscritos() {
        return inscritos;
    }

    public void setInscritos(Double inscritos) {
        this.inscritos = inscritos;
    }

    public Double getCc() {
        return cc;
    }

    public void setCc(Double cc) {
        this.cc = cc;
    }

    public Double getFpv() {
        return fpv;
    }

    public void setFpv(Double fpv) {
        this.fpv = fpv;
    }

    public Double getMts() {
        return mts;
    }

    public void setMts(Double mts) {
        this.mts = mts;
    }

    public Double getUcs() {
        return ucs;
    }

    public void setUcs(Double ucs) {
        this.ucs = ucs;
    }

    public Double getMas() {
        return mas;
    }

    public void setMas(Double mas) {
        this.mas = mas;
    }

    public Double getF21() {
        return f21;
    }

    public void setF21(Double f21) {
        this.f21 = f21;
    }

    public Double getPdc() {
        return pdc;
    }

    public void setPdc(Double pdc) {
        this.pdc = pdc;
    }

    public Double getMnr() {
        return mnr;
    }

    public void setMnr(Double mnr) {
        this.mnr = mnr;
    }

    public Double getPan() {
        return pan;
    }

    public void setPan(Double pan) {
        this.pan = pan;
    }

    public Double getValidos() {
        return validos;
    }

    public void setValidos(Double validos) {
        this.validos = validos;
    }

    public Double getBlancos() {
        return blancos;
    }

    public void setBlancos(Double blancos) {
        this.blancos = blancos;
    }

    public Double getNulos() {
        return nulos;
    }

    public void setNulos(Double nulos) {
        this.nulos = nulos;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getPais() {
        return pais;
    }

    public void setPais(String pais) {
        this.pais = pais;
    }

    public String getDepartamento() {
        return departamento;
    }

    public void setDepartamento(String departamento) {
        this.departamento = departamento;
    }

    public String getProvincia() {
        return provincia;
    }

    public void setProvincia(String provincia) {
        this.provincia = provincia;
    }

    public String getMncpo() {
        return mncpo;
    }

    public void setMncpo(String mncpo) {
        this.mncpo = mncpo;
    }

    public String getLocalidad() {
        return localidad;
    }

    public void setLocalidad(String localidad) {
        this.localidad = localidad;
    }

    public String getRcnto() {
        return rcnto;
    }

    public void setRcnto(String rcnto) {
        this.rcnto = rcnto;
    }

    public void setRecinto(Recinto recinto) {
        this.recinto = recinto;
    }
    public Recinto getRecinto() {
        return recinto;
    }

    public void setMunicipio(Municipio municipio) {
        this.municipio = municipio;
    }
    public Municipio getMunicipio() {
        return municipio;
    }


  

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Acta)) {
            return false;
        }
        Acta other = (Acta) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "elecciones.modelo.Acta[ id=" + id + " ]";
    }
    
}
