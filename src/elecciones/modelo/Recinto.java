/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package elecciones.modelo;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author RDuran
 */
@Entity
@Table(name = "recinto")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Recinto.findAll", query = "SELECT r FROM Recinto r"),
    @NamedQuery(name = "Recinto.findById", query = "SELECT r FROM Recinto r WHERE r.id = :id"),
    @NamedQuery(name = "Recinto.findByNombre", query = "SELECT r FROM Recinto r WHERE r.nombre = :nombre"),
    @NamedQuery(name = "Recinto.findByInscritos", query = "SELECT r FROM Recinto r WHERE r.inscritos = :inscritos"),
    @NamedQuery(name = "Recinto.findByCc", query = "SELECT r FROM Recinto r WHERE r.cc = :cc"),
    @NamedQuery(name = "Recinto.findByFpv", query = "SELECT r FROM Recinto r WHERE r.fpv = :fpv"),
    @NamedQuery(name = "Recinto.findByMts", query = "SELECT r FROM Recinto r WHERE r.mts = :mts"),
    @NamedQuery(name = "Recinto.findByUcs", query = "SELECT r FROM Recinto r WHERE r.ucs = :ucs"),
    @NamedQuery(name = "Recinto.findByMas", query = "SELECT r FROM Recinto r WHERE r.mas = :mas"),
    @NamedQuery(name = "Recinto.findByF21", query = "SELECT r FROM Recinto r WHERE r.f21 = :f21"),
    @NamedQuery(name = "Recinto.findByPdc", query = "SELECT r FROM Recinto r WHERE r.pdc = :pdc"),
    @NamedQuery(name = "Recinto.findByMnr", query = "SELECT r FROM Recinto r WHERE r.mnr = :mnr"),
    @NamedQuery(name = "Recinto.findByPan", query = "SELECT r FROM Recinto r WHERE r.pan = :pan"),
    @NamedQuery(name = "Recinto.findByValidos", query = "SELECT r FROM Recinto r WHERE r.validos = :validos"),
    @NamedQuery(name = "Recinto.findByBlancos", query = "SELECT r FROM Recinto r WHERE r.blancos = :blancos"),
    @NamedQuery(name = "Recinto.findByNulos", query = "SELECT r FROM Recinto r WHERE r.nulos = :nulos")})
public class Recinto implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)        
    private Integer id;
    @Column(name = "nombre")
    private String nombre;
    @Column(name = "inscritos")
    private Double inscritos;
    @Column(name = "cc")
    private Double cc;
    @Column(name = "fpv")
    private Double fpv;
    @Column(name = "mts")
    private Double mts;
    @Column(name = "ucs")
    private Double ucs;
    @Column(name = "mas")
    private Double mas;
    @Column(name = "f21")
    private Double f21;
    @Column(name = "pdc")
    private Double pdc;
    @Column(name = "mnr")
    private Double mnr;
    @Column(name = "pan")
    private Double pan;
    @Column(name = "validos")
    private Double validos;
    @Column(name = "blancos")
    private Double blancos;
    @Column(name = "nulos")
    private Double nulos;
    @Column(name = "estado")
    private String estado;
    @JoinColumn(name = "id_municipio", referencedColumnName = "id")
    @ManyToOne
    private Municipio municipio;

    public Recinto() {
    }

    public Recinto(String r) {
        nombre=r;
    }

    public Recinto(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Double getInscritos() {
        return inscritos;
    }

    public void setInscritos(Double inscritos) {
        this.inscritos = inscritos;
    }

    public Double getCc() {
        return cc;
    }

    public void setCc(Double cc) {
        this.cc = cc;
    }

    public Double getFpv() {
        return fpv;
    }

    public void setFpv(Double fpv) {
        this.fpv = fpv;
    }

    public Double getMts() {
        return mts;
    }

    public void setMts(Double mts) {
        this.mts = mts;
    }

    public Double getUcs() {
        return ucs;
    }

    public void setUcs(Double ucs) {
        this.ucs = ucs;
    }

    public Double getMas() {
        return mas;
    }

    public void setMas(Double mas) {
        this.mas = mas;
    }

    public Double getF21() {
        return f21;
    }

    public void setF21(Double f21) {
        this.f21 = f21;
    }

    public Double getPdc() {
        return pdc;
    }

    public void setPdc(Double pdc) {
        this.pdc = pdc;
    }

    public Double getMnr() {
        return mnr;
    }

    public void setMnr(Double mnr) {
        this.mnr = mnr;
    }

    public Double getPan() {
        return pan;
    }

    public void setPan(Double pan) {
        this.pan = pan;
    }

    public Double getValidos() {
        return validos;
    }

    public void setValidos(Double validos) {
        this.validos = validos;
    }

    public Double getBlancos() {
        return blancos;
    }

    public void setBlancos(Double blancos) {
        this.blancos = blancos;
    }

    public Double getNulos() {
        return nulos;
    }

    public void setNulos(Double nulos) {
        this.nulos = nulos;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public Municipio getMunicipio() {
        return municipio;
    }

    public void setMunicipio(Municipio municipio) {
        this.municipio = municipio;
    }


    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Recinto)) {
            return false;
        }
        Recinto other = (Recinto) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "elecciones.modelo.Recinto[ id=" + id + " ]";
    }
    
}
