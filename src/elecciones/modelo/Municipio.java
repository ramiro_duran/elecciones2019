/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package elecciones.modelo;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author RDuran
 */
@Entity
@Table(name = "municipio")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Municipio.findAll", query = "SELECT m FROM Municipio m"),
    @NamedQuery(name = "Municipio.findById", query = "SELECT m FROM Municipio m WHERE m.id = :id"),
    @NamedQuery(name = "Municipio.findByNombre", query = "SELECT m FROM Municipio m WHERE m.nombre = :nombre"),
    @NamedQuery(name = "Municipio.findByInscritos", query = "SELECT m FROM Municipio m WHERE m.inscritos = :inscritos"),
    @NamedQuery(name = "Municipio.findByCc", query = "SELECT m FROM Municipio m WHERE m.cc = :cc"),
    @NamedQuery(name = "Municipio.findByFpv", query = "SELECT m FROM Municipio m WHERE m.fpv = :fpv"),
    @NamedQuery(name = "Municipio.findByMts", query = "SELECT m FROM Municipio m WHERE m.mts = :mts"),
    @NamedQuery(name = "Municipio.findByUcs", query = "SELECT m FROM Municipio m WHERE m.ucs = :ucs"),
    @NamedQuery(name = "Municipio.findByMas", query = "SELECT m FROM Municipio m WHERE m.mas = :mas"),
    @NamedQuery(name = "Municipio.findByF21", query = "SELECT m FROM Municipio m WHERE m.f21 = :f21"),
    @NamedQuery(name = "Municipio.findByPdc", query = "SELECT m FROM Municipio m WHERE m.pdc = :pdc"),
    @NamedQuery(name = "Municipio.findByMnr", query = "SELECT m FROM Municipio m WHERE m.mnr = :mnr"),
    @NamedQuery(name = "Municipio.findByPan", query = "SELECT m FROM Municipio m WHERE m.pan = :pan"),
    @NamedQuery(name = "Municipio.findByValidos", query = "SELECT m FROM Municipio m WHERE m.validos = :validos"),
    @NamedQuery(name = "Municipio.findByBlancos", query = "SELECT m FROM Municipio m WHERE m.blancos = :blancos"),
    @NamedQuery(name = "Municipio.findByNulos", query = "SELECT m FROM Municipio m WHERE m.nulos = :nulos"),
    @NamedQuery(name = "Municipio.findByEstado", query = "SELECT m FROM Municipio m WHERE m.estado = :estado")})
public class Municipio implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)        
    private Integer id;
    @Column(name = "nombre")
    private String nombre;
    @Column(name = "inscritos")
    private Double inscritos;
    @Column(name = "cc")
    private Double cc;
    @Column(name = "fpv")
    private Double fpv;
    @Column(name = "mts")
    private Double mts;
    @Column(name = "ucs")
    private Double ucs;
    @Column(name = "mas")
    private Double mas;
    @Column(name = "f21")
    private Double f21;
    @Column(name = "pdc")
    private Double pdc;
    @Column(name = "mnr")
    private Double mnr;
    @Column(name = "pan")
    private Double pan;
    @Column(name = "validos")
    private Double validos;
    @Column(name = "blancos")
    private Double blancos;
    @Column(name = "nulos")
    private Double nulos;
    @Column(name = "estado")
    private String estado;

    public Municipio() {
    }

    public Municipio(String m) {
        this.nombre=m;
    }
    
    public Municipio(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Double getInscritos() {
        return inscritos;
    }

    public void setInscritos(Double inscritos) {
        this.inscritos = inscritos;
    }

    public Double getCc() {
        return cc;
    }

    public void setCc(Double cc) {
        this.cc = cc;
    }

    public Double getFpv() {
        return fpv;
    }

    public void setFpv(Double fpv) {
        this.fpv = fpv;
    }

    public Double getMts() {
        return mts;
    }

    public void setMts(Double mts) {
        this.mts = mts;
    }

    public Double getUcs() {
        return ucs;
    }

    public void setUcs(Double ucs) {
        this.ucs = ucs;
    }

    public Double getMas() {
        return mas;
    }

    public void setMas(Double mas) {
        this.mas = mas;
    }

    public Double getF21() {
        return f21;
    }

    public void setF21(Double f21) {
        this.f21 = f21;
    }

    public Double getPdc() {
        return pdc;
    }

    public void setPdc(Double pdc) {
        this.pdc = pdc;
    }

    public Double getMnr() {
        return mnr;
    }

    public void setMnr(Double mnr) {
        this.mnr = mnr;
    }

    public Double getPan() {
        return pan;
    }

    public void setPan(Double pan) {
        this.pan = pan;
    }

    public Double getValidos() {
        return validos;
    }

    public void setValidos(Double validos) {
        this.validos = validos;
    }

    public Double getBlancos() {
        return blancos;
    }

    public void setBlancos(Double blancos) {
        this.blancos = blancos;
    }

    public Double getNulos() {
        return nulos;
    }

    public void setNulos(Double nulos) {
        this.nulos = nulos;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Municipio)) {
            return false;
        }
        Municipio other = (Municipio) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "elecciones.modelo.Municipio[ id=" + id + " ]";
    }
    
}
