package  elecciones.vista;

import elecciones.modelo.Municipio;
import elecciones.modelo.Recinto;
import java.io.File;
import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
 
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
 
public class ActasExcel {
    private String nombreArchivo = "actas.xlsx";
    private String rutaArchivo = "C:\\Users\\RDuran\\Documents\\NetBeansProjects\\Elecciones2019\\resources\\" + nombreArchivo;
    private String hoja = "Data";
    private String pais, depto, provincia, municipio, localidad, recinto, codigoMesa, eleccion, circun, strvar;
    private Double nroDepto=0.0, nroMunicipio=0.0, circunscripcion, nroMesa=0.0, inscritos, cc,fpv, mts, ucs,mas, f21, pdc, mnr,pan, validos, blancos, nulos, estado;
    private Municipio mnpo;
    private Recinto rcnto;
       
    public void ActasExcel(){
            
   } 

    public List<Municipio> getMunicipios() {
        List<Municipio> listaMunicipos = new ArrayList<Municipio>();
        List<String> lista = new ArrayList<String>();
		try (FileInputStream file = new FileInputStream(new File(rutaArchivo))) {
			// leer archivo excel
			XSSFWorkbook worbook = new XSSFWorkbook(file);
			//obtener la hoja que se va leer
			XSSFSheet sheet = worbook.getSheetAt(0);
			//obtener todas las filas de la hoja excel
			Iterator<Row> rowIterator = sheet.iterator();
			Row row;
                        // Se recorre los campos de la fila del titulo
                        if (rowIterator.hasNext()) {
                                row = rowIterator.next();
                        }
                        // se recorre las demas filas de datos hasta el final
			while (rowIterator.hasNext()) {
				row = rowIterator.next();
				//se obtiene las celdas por fila
				Iterator<Cell> cellIterator = row.cellIterator();
				Cell cell;
				//se recorre cada celda
				while (cellIterator.hasNext()) {
					// se obtiene la celda en específico y se la imprime
					cell = cellIterator.next();  pais = cell.getStringCellValue();
                                        cell = cellIterator.next();  nroDepto = cell.getNumericCellValue(); 
                                        cell = cellIterator.next();  depto = cell.getStringCellValue();
                                        cell = cellIterator.next();  provincia = cell.getStringCellValue();
                                        cell = cellIterator.next();  nroMunicipio = cell.getNumericCellValue();
                                        cell = cellIterator.next();  municipio = cell.getStringCellValue();
                                        cell = cellIterator.next();  circun="";
                                        cell = cellIterator.next();  localidad = cell.getStringCellValue();
                                        cell = cellIterator.next();  recinto = cell.getStringCellValue();
                                        cell = cellIterator.next();  nroMesa = cell.getNumericCellValue();
                                        cell = cellIterator.next();  codigoMesa = cell.getStringCellValue();// codigo mesa
                                        cell = cellIterator.next();  eleccion = cell.getStringCellValue();
                                        cell = cellIterator.next();  inscritos=cell.getNumericCellValue();
                                        cell = cellIterator.next(); cc=cell.getNumericCellValue();
                                        cell = cellIterator.next(); fpv=cell.getNumericCellValue();
                                        cell = cellIterator.next(); mts=cell.getNumericCellValue();
                                        cell = cellIterator.next(); ucs=cell.getNumericCellValue() ;
                                        cell = cellIterator.next(); mas=cell.getNumericCellValue();
                                        cell = cellIterator.next(); f21=cell.getNumericCellValue();
                                        cell = cellIterator.next(); pdc=cell.getNumericCellValue();
                                        cell = cellIterator.next(); mnr=cell.getNumericCellValue();
                                        cell = cellIterator.next(); pan=cell.getNumericCellValue();
                                        cell = cellIterator.next(); validos=cell.getNumericCellValue();
                                        cell = cellIterator.next(); blancos=cell.getNumericCellValue();
                                        cell = cellIterator.next(); nulos=cell.getNumericCellValue();
                                        cell = cellIterator.next(); estado=cell.getNumericCellValue();
				}
                                if (!lista.contains(municipio))
                                            lista.add(municipio);
			}
		} catch (Exception e) {
			System.out.print(e.getMessage());
		}            
        for(String item:lista){
            mnpo = new Municipio(item);
            listaMunicipos.add(mnpo);
        }
        return listaMunicipos;
    }

    public List<Recinto> getRecintos() {
        List<Recinto> listaRecintos = new ArrayList<Recinto>();
        List<String> listaR = new ArrayList<String>();
        List<String> listaM = new ArrayList<String>();
		try (FileInputStream file = new FileInputStream(new File(rutaArchivo))) {
			// leer archivo excel
			XSSFWorkbook worbook = new XSSFWorkbook(file);
			//obtener la hoja que se va leer
			XSSFSheet sheet = worbook.getSheetAt(0);
			//obtener todas las filas de la hoja excel
			Iterator<Row> rowIterator = sheet.iterator();
			Row row;
                        // Se recorre los campos de la fila del titulo
                        if (rowIterator.hasNext()) {
                                row = rowIterator.next();
                        }
                        // se recorre las demas filas de datos hasta el final
			while (rowIterator.hasNext()) {
				row = rowIterator.next();
				//se obtiene las celdas por fila
				Iterator<Cell> cellIterator = row.cellIterator();
				Cell cell;
				//se recorre cada celda
				while (cellIterator.hasNext()) {
					// se obtiene la celda en específico y se la imprime
					cell = cellIterator.next();  pais = cell.getStringCellValue();
                                        cell = cellIterator.next();  nroDepto = cell.getNumericCellValue(); 
                                        cell = cellIterator.next();  depto = cell.getStringCellValue();
                                        cell = cellIterator.next();  provincia = cell.getStringCellValue();
                                        cell = cellIterator.next();  nroMunicipio = cell.getNumericCellValue();
                                        cell = cellIterator.next();  municipio = cell.getStringCellValue();
                                        cell = cellIterator.next();  circun="";
                                        cell = cellIterator.next();  localidad = cell.getStringCellValue();
                                        cell = cellIterator.next();  recinto = cell.getStringCellValue();
                                        cell = cellIterator.next();  nroMesa = cell.getNumericCellValue();
                                        cell = cellIterator.next();  codigoMesa = cell.getStringCellValue();// codigo mesa
                                        cell = cellIterator.next();  eleccion = cell.getStringCellValue();
                                        cell = cellIterator.next();  inscritos=cell.getNumericCellValue();
                                        cell = cellIterator.next(); cc=cell.getNumericCellValue();
                                        cell = cellIterator.next(); fpv=cell.getNumericCellValue();
                                        cell = cellIterator.next(); mts=cell.getNumericCellValue();
                                        cell = cellIterator.next(); ucs=cell.getNumericCellValue() ;
                                        cell = cellIterator.next(); mas=cell.getNumericCellValue();
                                        cell = cellIterator.next(); f21=cell.getNumericCellValue();
                                        cell = cellIterator.next(); pdc=cell.getNumericCellValue();
                                        cell = cellIterator.next(); mnr=cell.getNumericCellValue();
                                        cell = cellIterator.next(); pan=cell.getNumericCellValue();
                                        cell = cellIterator.next(); validos=cell.getNumericCellValue();
                                        cell = cellIterator.next(); blancos=cell.getNumericCellValue();
                                        cell = cellIterator.next(); nulos=cell.getNumericCellValue();
                                        cell = cellIterator.next(); estado=cell.getNumericCellValue();
				}
                                if (!listaR.contains(recinto)) {
                                            listaR.add(recinto);
                                            listaM.add(municipio);
                                }
			}
		} catch (Exception e) {
			System.out.print(e.getMessage());
		}            
        for(int k=0; k<listaR.size(); k++){
            rcnto = new Recinto(listaR.get(k));
            rcnto.setMunicipio(new Municipio(listaM.get(k)));
            listaRecintos.add(rcnto);
        }
        return listaRecintos;
    }
    
    public void  listarActas(){
        System.out.println("RECUPERANDO ACTAS");
 		try (FileInputStream file = new FileInputStream(new File(rutaArchivo))) {
			// leer archivo excel
			XSSFWorkbook worbook = new XSSFWorkbook(file);
			//obtener la hoja que se va leer
			XSSFSheet sheet = worbook.getSheetAt(0);
			//obtener todas las filas de la hoja excel
			Iterator<Row> rowIterator = sheet.iterator();
 
			Row row;
                        // Se recorre los campos de la fila del titulo
                        if (rowIterator.hasNext()) {
                                row = rowIterator.next();
                                Iterator<Cell> cellIterator = row.cellIterator();
				Cell cell;                            
				while (cellIterator.hasNext()) {
                                    cell = cellIterator.next();
                                    System.out.print(cell.getStringCellValue()+" | ");
                                }
       				System.out.println();
                        }
                        // se recorre las demas filas de datos hasta el final
			while (rowIterator.hasNext()) {
				row = rowIterator.next();
				//se obtiene las celdas por fila
				Iterator<Cell> cellIterator = row.cellIterator();
				Cell cell;
				//se recorre cada celda
				while (cellIterator.hasNext()) {
					// se obtiene la celda en específico y se la imprime
					cell = cellIterator.next();  pais = cell.getStringCellValue();
                                        System.out.print(pais+" | ");
                                        cell = cellIterator.next();  nroDepto = cell.getNumericCellValue(); 
                                        System.out.print(nroDepto+" | ");
                                        cell = cellIterator.next();  depto = cell.getStringCellValue();
                                        System.out.print(depto+" | ");
                                        cell = cellIterator.next();  provincia = cell.getStringCellValue();
                                        System.out.print(provincia+" | ");
                                        cell = cellIterator.next();  nroMunicipio = cell.getNumericCellValue();
                                        System.out.print(nroMunicipio+" | ");
                                        cell = cellIterator.next();  municipio = cell.getStringCellValue();
                                        System.out.print(municipio+" | ");
                                        cell = cellIterator.next();  circun="";
                                        System.out.print(circun+" | ");
                                        cell = cellIterator.next();  localidad = cell.getStringCellValue();
                                        System.out.print(localidad+" | ");
                                        cell = cellIterator.next();  recinto = cell.getStringCellValue();
                                        System.out.print(recinto+" | ");
                                        cell = cellIterator.next();  nroMesa = cell.getNumericCellValue();
                                        System.out.print(nroMesa+" | ");
                                        cell = cellIterator.next();  codigoMesa = cell.getStringCellValue();// codigo mesa
                                        System.out.print(codigoMesa+" | ");
                                        cell = cellIterator.next();  eleccion = cell.getStringCellValue();
                                        System.out.print(eleccion+" | ");
                                        cell = cellIterator.next();  inscritos=cell.getNumericCellValue();
                                        System.out.print(inscritos+" | ");
                                        cell = cellIterator.next(); cc=cell.getNumericCellValue();
                                        cell = cellIterator.next(); fpv=cell.getNumericCellValue();
                                        cell = cellIterator.next(); mts=cell.getNumericCellValue();
                                        cell = cellIterator.next(); ucs=cell.getNumericCellValue() ;
                                        cell = cellIterator.next(); mas=cell.getNumericCellValue();
                                        cell = cellIterator.next(); f21=cell.getNumericCellValue();
                                        cell = cellIterator.next(); pdc=cell.getNumericCellValue();
                                        cell = cellIterator.next(); mnr=cell.getNumericCellValue();
                                        cell = cellIterator.next(); pan=cell.getNumericCellValue();
                                        cell = cellIterator.next(); validos=cell.getNumericCellValue();
                                        cell = cellIterator.next(); blancos=cell.getNumericCellValue();
                                        cell = cellIterator.next(); nulos=cell.getNumericCellValue();
                                        cell = cellIterator.next(); estado=cell.getNumericCellValue();
				}
				System.out.println();
			}
		} catch (Exception e) {
			System.out.print(e.getMessage());
		}            
        }
}