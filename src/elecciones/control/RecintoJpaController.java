/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package elecciones.control;

import elecciones.control.exceptions.NonexistentEntityException;
import elecciones.modelo.Recinto;
import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.Persistence;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

/**
 *
 * @author RDuran
 */
public class RecintoJpaController implements Serializable {

    public RecintoJpaController() {
        emf = Persistence.createEntityManagerFactory("EleccionesPU");        
    }
    public RecintoJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Recinto recinto) {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            em.persist(recinto);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Recinto recinto) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            recinto = em.merge(recinto);
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = recinto.getId();
                if (findRecinto(id) == null) {
                    throw new NonexistentEntityException("The recinto with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Recinto recinto;
            try {
                recinto = em.getReference(Recinto.class, id);
                recinto.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The recinto with id " + id + " no longer exists.", enfe);
            }
            em.remove(recinto);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Recinto> findRecintoEntities() {
        return findRecintoEntities(true, -1, -1);
    }

    public List<Recinto> findRecintoEntities(int maxResults, int firstResult) {
        return findRecintoEntities(false, maxResults, firstResult);
    }

    private List<Recinto> findRecintoEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Recinto.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Recinto findRecinto(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Recinto.class, id);
        } finally {
            em.close();
        }
    }

    public Recinto buscarRecinto(String nombre) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Recinto.class, nombre);
        } finally {
            em.close();
        }
    }
    
    
    public int getRecintoCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Recinto> rt = cq.from(Recinto.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
